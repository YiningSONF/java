public abstract class  Account {
    //property
    private double balance;
    private String name;
    private static double interestRate = 1.1;

    //constructor
    public Account(String s, double d) {
        name = s;
        balance = d;
        
    }

    public Account() {
        name = "Yining";
        balance = 50;
        
    }

    //getter and setter
    public void setBalance(double d) {
        balance = d;  
    }
    public double getBalance() {
        return balance;
    }
    public void setName(String s) {
        name = s;
    }
    public String getName() {
        return name;
    }

    public static double getInterestRate() {
        return interestRate;
    } 

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    } 

    //methods
    public abstract void addInterest();

    public boolean withdraw(double amount) {
        if(balance < amount) return false;
        balance -= amount;
        return true;
    }

    public boolean withdraw() {
        if(balance-20<0) return false;
        balance -= 20;
        return true;   
    }

    

    

    
}

