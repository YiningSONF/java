public class TestAccount2 {
    public static void printWithdraw (Account a, boolean b) {
        String s = a.getName();
        if(b) {
            System.out.println(s + " withdraw successfully");
        }
        else {
            System.out.println(s + " withdraw failed.");
        }
        
    }
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        arrayOfAccounts[0] = new Account();
        arrayOfAccounts[1] = new Account("Harry", 10);
        arrayOfAccounts[2] = new Account("Lily", 222222);
        arrayOfAccounts[3] = new Account("James", 9999);
        arrayOfAccounts[4] = new Account("Sirius", 290112);
        for (int i = 0; i < arrayOfAccounts.length; i++) {
            System.out.println("Name: " + arrayOfAccounts[i].getName() +
             ", Balance: " + arrayOfAccounts[i].getBalance());
             arrayOfAccounts[i].addInterest();
             System.out.println("Name: " + arrayOfAccounts[i].getName() + 
            ", New Balance: " + arrayOfAccounts[i].getBalance());
        }

        
        printWithdraw(arrayOfAccounts[0], arrayOfAccounts[0].withdraw(100));
        printWithdraw(arrayOfAccounts[1], arrayOfAccounts[1].withdraw());
        printWithdraw(arrayOfAccounts[2], arrayOfAccounts[2].withdraw(22222));
        printWithdraw(arrayOfAccounts[3], arrayOfAccounts[3].withdraw(90));

    }
}