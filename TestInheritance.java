public class TestInheritance {
    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[3];
        arrayOfAccounts[0] = new SavingsAccount("Serena", 2);
        arrayOfAccounts[1] = new CurrentAccount("Ann", 4);
        arrayOfAccounts[2] = new SavingsAccount("Lucy", 6);
        for (int i = 0; i < arrayOfAccounts.length; i++) {
            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getName() + " balance: "+ arrayOfAccounts[i].getBalance());
        }
    }
}